# PrivateTracer
Here, we are co-creating the technical foundation for a privacy-preserving proximity tracing app. This does not mean we have concluded that implementing such an app is the best and only idea. We are simply convinced that the only way to find out is to build and test it.

## Context
We are currently focused on developing a digital solution to help improve the impact of contact tracing within The Netherlands. We are actively communicating with the Ministry of Health, awaiting further decisions. On our website you will find most of the information required (in Dutch). We have [press releases](https://www.privatetracer.org/press) and [statements](https://www.privatetracer.org/nieuws) available (in Dutch).

## Who are we?
See our [Community Overview](Onboarding - Getting to Know PrivateTracer/Community Overview.md) for everybody involved.

## What is our goal?
Our goal is to find out if a contact tracing app can be an effective tool in battling the COVID-19 virus and provide healthcare organisations with support in their contact tracing activities, that will allow our societies to be opened up responsibly. We want to achieve this in cooperation with healthcare organisations and employees, (local) governments, companies and civil society organisations. We want to develop and test such an application on the basis of sharp analysis, critical questioning and improvements wherever possible. Anyone can contribute to battle COVID-19 and only together we will be able to control the virus.

## How can you help?
Please see our [Getting Started with PrivateTracer](Onboarding - Getting to Know PrivateTracer/README.md) for more information getting started.
You can browse all our current issues via [this link](https://gitlab.com/groups/PrivateTracer/-/boards/1641202). You can also filter on the different repositories that are being developed.

If you have an unanswered question, or want to help in other ways, please send an email to contribute@privatetracer.org

## Key principles
The chosen approach has the following key aspects 
*  Privacy by Design: The recording of interactions, and how this is then used to prevent the spread of the virus, is done with privacy as the basis for the fundamental design. 
*  Proximity Tracking: the solution focuses on local interactions. All data captured is stored securely on peoples mobile phones and not on a central server.
*  Notifications: all matching of interactions should be done on local devices.
*  Open source: all code created from the solution would be open source, thereby helping to build a wider community to develop the approach further and make it accessible to every country.
*  Self sovereign: For the storage of interactions, this would be done in a secure repository on the device. The information that is exchanged in an interaction would leverage the DP-3T protocol. The user remains at all times in full control of their data. 

The above approach we believe, because we focus on citizen empowerment driven by technology designed with privacy and data sovereignty in mind. We do this by using a distributed identity privacy protocol and stimulating governments worldwide to strengthen their relationship with their citizens.


## What is the biggest barrier for progress?
The risk of infection from a particular interaction is an area of active research. The key aspects for determining the infection risk are:
* **Proximity**: How close was the user to another user, and for how long
* **Environment**: Where did the interaction take place? It is established that interactions indoor have a higher risk than interactions outdoor
There are already deployed solution that leverage Bluetooth LE for detecting locally devices in close proximity, and the duration of such proximity can also be tracked. 

However additional **calibration** is also needed to be able to take the raw Bluetooth data and turn it into information related to the distance of the other device - and to what end this can be of sufficent resolutioni. Further, this data may need to be combined with meta information about the location to derive an actual risk of infection - which ultimately is what is important. 

It is our belief that detecting the actual infection risk from an interaction is a topic that will require wide collaboration - from ensuring dominant client OS (e.g. iOS, Android) are updated to faciliate the interactions needed at the reliability needed, calibration per device pair to proximity data as well as what other data sources (e.g. local gps data that is already known to the device) are used to determine the environment where the interaction took place. 

## Further information
Please see the following documents for additional information and how to engage with the project:
* [Technical Design](./TechnicalDesign.md) - For more information on the underlying design of PrivateTracer
* [Development Setup](Development Documentation/Development Setup.md) - For information on how the project is structured, top level repositories, roles, etc
* [Use cases](usecases): For descriptions, interaction diagrams etc for key use cases related to privacy preserving contact tracing.
* [Security](./Security/README.md): Security related information.
* [Getting Started](https://gitlab.com/PrivateTracer/coordination/-/blob/master/Onboarding%20-%20Getting%20to%20Know%20PrivateTracer/README.md) - For developers

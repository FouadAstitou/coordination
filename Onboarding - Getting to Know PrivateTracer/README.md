# How do we onboard new developers?
Everybody is invited and welcome to contribute to PrivateTracer. There is much do...

There are 5 steps to be followed before you can join the PrivateTracer project.

* Read all relevant documentation in the onboarding directory and [the technical design](TechnicalDesign.md).
* Check the issue [board](https://gitlab.com/groups/PrivateTracer/-/boards/1641202) for issues you can help with, or think of issues that could be added.
* Reach out to contribute@privatetracer.org with your contribution suggestions
* Have a 15 min call with our Community Manager
* Have a final 15 min call with our CTO or repository lead
* You're in!


# How to contribute to PrivateTracer

The process is straight-forward.

* Read [How to get faster merge request reviews (example by Kubernetics)](https://github.com/kubernetes/community/blob/master/contributors/guide/pull-requests.md#best-practices-for-faster-reviews) (but skip step 0 and 1)
* Fork the PrivateTracer repositories you want to work on
* Only work on issues put up on the [board](https://gitlab.com/groups/PrivateTracer/-/boards/1641202) or create your own issue. Read [this](Gitlab%20issue%20board.md) to get an idea how it is used.
* Ensure tests work.
* Sign the [Contributor License Agreement](../cla/readme.md)

## Daily Standup
The PrivateTracer team has a daily standup at 9:00 (CEST). After the official onboarding you will be invited to our chat and you will receive the standup link.

## Feature suggestions

If you want to suggest a new feature for PrivateTracer (e.g., new integrations), please send an email to contribute@privatetracer.org, also if you have any other open questions.

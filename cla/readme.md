# Private Tracer Contributor License Agreement

## Instructions

To certify that contributors have the right to submit their contribtions to the Private Tracer project we ask them to sign our [*Contributor License Agreement (CLA)*](./cla.md). In order to simplify this process and align it with the contributor workflow, we are asking contributors to fill in the CLA and sign the commit using their PGP key.

Please use follow the following steps:
1. fork the coordination repo (unless you have developer access to the project already) and create a new local branch in the coordination repo named: 'cla-*gitlab-account*', where you substitute *gitlab-account* with your Gitlab account name
2. fill in the [standard CLA](./cla.md) with your name, place of signing and Gitlab account and save in the `signed` folder as `cla-gitlab-account.md` (and yes, substitute *gitlab-account* with your Gitlab account name)
3. add the file using `git add`
4. sign the commit using your PGP key: `git commit -S -m "signed CLA gitlab-account"`
5. push the commit to the Gitlab coordination repository
6. create a merge request to merge your signed cla with the master branch.

## Problems

If you see:
```bash
error: cannot run gpg: No such file or directory
error: gpg failed to sign the data
fatal: failed to write commit object
```
you can try looking at the value of `GPG_TTY`. You have this set automatically by adding:
```bash
export GPG_TTY=$(tty)
```
to your `~/.bash_profile`, `~/.profile` or `~/.bashrc`.

## References

- [Signing commits with GPG in Gitlab](https://docs.gitlab.com/ee/user/project/repository/gpg_signed_commits/)
- [Managing OpenPGP Keys in Linux/windows](https://riseup.net/en/security/message-security/openpgp/gpg-keys)
- [The Difference Between PGP, OpenPGP, and GnuPG Encryption](https://blog.ipswitch.com/the-difference-between-pgp-openpgp-and-gnupg-encryption)
- [How to impress your security friends - advanced users](https://github.com/drduh/YubiKey-Guide)

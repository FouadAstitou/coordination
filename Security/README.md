# Security 

> Instructions for '**reponsible disclosure**' (see also below): [link](../ResponsibleDisclosure.md)

PrivateTracer follows the 'Privacy By Design' principle, by building privacy into its design (link: https://www.privatetracer.org/privacy-by-design).

We know that there is no privacy without security and therefor we are leveraging the expertise of our partners and the power of the community in this area.

We include a Coordinated Vulnerability Disclosure policy in the PrivateTracer application to enable users and ethical hackers to report vulnerabilities. reports originating from the Coordinated Vulnerability Disclosure workflow are reviewed by a triage team of security experts. The triage team assesses all submitted reports for validity and completeness. Only complete and validated reports are accepted and forwarded. Responsible Disclosure (RD) policy - known outside of The Netherlands as Coordinated Vulnerability Disclosure (CVD) policy,  will be handled through our security platform [link to CVD platform](https://app.zerocopter.com/en/rd/228522f0-5a89-4306-99a8-2f4342033fb9). The platform enables tracking and management of discovered vulnerabilities. We utilize the power of the security community to help secure PrivateTracer.

## Approach

We follow industry best practices in the areas of:

- the secure software development process, applying a unified framework - combining industry standards
- building security and privacy into the product - based on the ISO/IEC 25010 standard for product  quality

## Secure software development process

In order to safeguard security and privacy in PrivateTracer development, we apply a unified framework based on the leading existing standards in this area (eg. ISO/IEC 62443 and 27034, PCI-SSC, OWASP-SAMM, BSIMM, and Microsoft SDL). Recent research by SIG with ENISA has led to this framework, identifying a number of key activities (https://www.enisa.europa.eu/publications/advancing-software-security-through-the-eu-certification-framework).
	
In order to specify what elements are required in the process, the key activities from the unified framework are detailed by looking at the recently overhauled OWASP SAMM standard because 1) it is up to date, 2) it provides maturity levels, and 3) it provides practices for an agile way of working.

Based on these key activities, the next steps for PrivateTracer development are:
- **Training**: privacy aspects are quite important in this application and a typical blind spot in teams. Key privacy principles have been communicated to the development team.
- **Requirements**: We suggest ASVS in combination with MASVS. SIG recently was involved in research on gaps (as part of the Grip on SSD initiative) and concluded that this covers the most dominant standards in the Netherlands: NCSC richtlijnen voor mobiele applicaties , the Dutch BIO, ISO/IEC 27002 and Grip on SSD mobile.
Regarding privacy we suggest the SIGO ISO25100/29100 privacy model to be used as requirement set for privacy.
- **Coding guideline**s: In order to comply with the SIG ISO25010 security model, we recommend the Security guidance for producers using the SIG ISO25010 security model: https://www.softwareimprovementgroup.com/wp-content/uploads/2019/12/2019-SIG-Evaluation-Criteria-Security-Guidance-for-producers-V3.pdf
The SIG ISO25010 security model provides a uniform framework that builds on existing industry standards using the ISO 25010 reference frame.
- **Design and Design review**: Constantly being performed with security experts in the consortium.
- **Threat modelling**: Constantly being performed with security experts in the consortium.
- **Secure verification** The source code is being scanned and reviewed by the consortium partners (automated testing, static analysis tools, test tools, manual code review and penetration testing). Handling/triaging of CVD reports is handled through the software security platform. Good security verification includes the monitoring of software quality, which is in place, in order to [assess code quality,](https://gitlab.com/PrivateTracer/coordination/-/blob/master/SoftwareQualityStatus.md) since high code quality is a prerequisite for sustainable security and privacy.
- **Dependency management**: verification is covered through the software quality platform
- **Incident management**: to be specified
- **Vulnerability management**: through software security platform
- **Environment hardening:** A constant topic in design review and threat modelling.



## Acknowledgements

We are supported by the software quality and security platforms from our partners:

- [SIG](https://sig.eu) - Sigrid software quality platform
- [Zerocopter](https://zerocopter.nl) - Software security platform

## References
- [Privacy and Security Attacks on Digital Proximity Tracing Systems - DP3-T](https://github.com/DP-3T/documents/blob/master/Security%20analysis/Privacy%20and%20Security%20Attacks%20on%20Digital%20Proximity%20Tracing%20Systems.pdf)






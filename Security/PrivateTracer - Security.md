# PrivateTracer Security Overview
This is a work in progress, any additions are welcome.

Disclaimer: If you find a vulnerability or privacy issue please contact us privately on gitlab or telegram. All such contributions will be looked at and will be credited (if one desires).

## Introduction
The main goal for this project is to ensure that users data is private and secure at all times. The goal of this paper is to look at the security topics surounding the PrivateTracer project. 

## Threat actors
It is always good to start by looking at who the threat actors might be for a particular system and what motivates them.

- Script kiddies / Opportunistic Criminals
  - Motivation is to cause disruption and make a "name" for themselves.
  - Political statements
- Organized Cyber Criminals
  - Gain information about individuals.
  - Us system as a way to target individuals.
  - Financial gains (e.g. malware)
- Nation States
  - Track individuals in other countries.
  - Abuse the system to spread false information.
  - Sabotage the system.

## Security of the system
The system needs to adhere to the C.I.A (confidentiality, integrity, and accessibility).
The client and server will use the STRIDE (Spoofing, Tampering, Repudiation, Denial of Service, Information Disclosure, and Elevation of Privilege) framework to model security threats on the client and server. 
Then for each component the CAPEC (Common Attack Pattern Enumeration and Classification)[1] framework will be used to describe the potential attack on the components.

### Client STRIDE
- Spoofing
  - Bluetooth data spoofing
  - Wifi data spoofing
- Tampering
  - Data manipulation on device
  - Data manipulation in transit to / from device
  - Incoming data validated
- Repudiation
  - Data collected and stored on the device must be checked for malicious manipulation
- Denial of Service
  - DDOS of client bluetooth (spam fake bluetooth mac addresses to pollute data)
  - DDOS of client Wifi 
- Information Disclosure
  - The keys and data collected are stored securely and privately 
  - Meta-data collected but not encrypted is removed
  - Only data needed should be logged
- Elevation of Privilege
  - Bluetooth data transfer [0]
  - Data retrieved from server is handled correctly


### Server STRIDE
- Spoofing
  - Clients are not able to upload spoofed data
- Tampering
  - Data uploaded/downloaded cannot be manipulated
  - Meta data on server is secured against tampering or removed
- Repudiation
  - Ensure that data uploaded is factual
- Denial of Service
  - Ensure that clients can upload/download data
  - Ensure that there are measures against DDOS
- Information Disclosure
  - Meta data on server is removed
- Elevation of Privilege
  - Secure all connections between client and server
  - Data is treated as none-executable
  - System should be running with latest patches

## Communication threat to consider
- Communication channel is not secure and data can be read or manipulated.

## Client threats to consider
- Client device is compromised and the keys / data are leaked. Client is the device that holds that data and the keys to that data. Keys should be treated as the most important asset. In the best case scenario keys should be generated and stored on a phones "secure enclave" and only be accessible (to the best effort) to our app.
- Meta-data around the collected / encrypted data is leaked. 
- Clients are flooded with "contact" connections (depending on the tech used, this could be bluetooth or wifi.). This will generate a lot of false data on the client device. 
- Exploit in the way that data is shared between devices, allowing for code execution.  
- The client receives and decryptes data that is then executed as malicious code. As the server cannot validate encrypted data, the client will need to be the one that validates data retrieved from the server. 

## Server threats to consider
- Server is attacked with  DDOS attack. We should assume that this will be a large target to "take down".
- Server is flooded with fake upload data. As calculated in the Traffic Scaling document there might be a lot of data (Up to 1Pb) that might be sent around. There needs to be a way to prevent people from uploading data arbitrary. One idea is to only upload data once a patient has been authorized by a One Time Code from a medical professional.   
- Server will be attacked and a weakness will be used to get access to the system / data. Depending on the scale of this project we might even want to consider advanced threats (i.e. nation states) but this should be looked at once that bridge is crossed. We should implement some type of logging / monitoring (we can make this public as well).
- Clients downloading/searching uploaded data to see patterns in meta-data (i.e. time uploaded, upload frequency, etc.) uploaded. This can be mitigated by the server now publishing all the results in one go. 

#### Client CAPEC 
- CAPEC 441 Malicious Logic Insertion
- CAPEC 466 Leveraging Active Man in the Middle Attacks to Bypass SameOrigin Policy
- CAPEC 604 Wi-Fi Jamming
- CAPEC 612 WiFi MAC Address Tracking
- CAPEC 613 WiFi SSID Tracking
- CAPEC 619 Signal Strength Tracking
- CAPEC 625 Mobile Device Fault Injection
- CAPEC 35 Leverage Executable Code in Non-Executable Files
- CAPEC 499 Intent Intercept
- CAPEC 500 WebView Injection
- CAPEC 501 Activity Hijack
- CAPEC 502 Intent Spoof
- CAPEC 498 Probe iOS Screenshots
- CAPEC 646 Peripheral Footprinting
- CAPEC 37 Retrieve Embedded Sensitive Data
- CAPEC 39 Manipulating Opaque Client-based Data Tokens
- CAPEC 441 Malicious Logic Insertion
- CAPEC 463 Padding Oracle Crypto Attack
- CAPEC 473 Signature Spoof
- CAPEC 474 Signature Spoofing by Key Theft
- CAPEC 475 Signature Spoofing by Improper Validation
- CAPEC 477 Signature Spoofing by Mixing Signed and Unsigned Content
- CAPEC 485 Signature Spoofing by Key Recreation
- CAPEC 609 Cellular Traffic Intercept
- CAPEC 625 Mobile Device Fault Injection
- CAPEC 636 Hiding Malicious Data or Code within Files
- CAPEC 639 Probe System Files
- CAPEC 94 Man in the Middle Attack
- CAPEC 97 Cryptanalysis

#### Server CAPEC
- CAPEC 217 Exploiting Incorrectly Configured SSL 
- CAPEC 220 Client-Server Protocol Manipulation 
- CAPEC 253 Remote Code Inclusion 
- CAPEC 256 SOAP Array Overflow 
- CAPEC 261 Fuzzing for garnering other adjacent user/sensitive data 
- CAPEC 267 Leverage Alternate Encoding 
- CAPEC 273 HTTP Response Smuggling 
- CAPEC 278 Web Services Protocol Manipulation 
- CAPEC 297 TCP ACK Ping 
- CAPEC 3 Using Leading 'Ghost' Character Sequences to Bypass InputFilters 1
- CAPEC 312 Active OS Fingerprinting 
- CAPEC 313 Passive OS Fingerprinting 
- CAPEC 320 TCP Timestamp Probe 
- CAPEC 36 Using Unpublished APIs 
- CAPEC 42 MIME Conversion 
- CAPEC 445 Malicious Logic Insertion into Product Software viaConfiguration Management Manipulation 1
- CAPEC 447 Design Alteration 
- CAPEC 460 HTTP Parameter Pollution (HPP) 
- CAPEC 47 Buffer Overflow via Parameter Expansion 
- CAPEC 479 Malicious Root Certificate 
- CAPEC 482 TCP Flood 
- CAPEC 487 ICMP Flood 
- CAPEC 489 SSL Flood 
- CAPEC 490 Amplification 
- CAPEC 495 UDP Fragmentation 
- CAPEC 533 Malicious Manual Software Update 
- CAPEC 558 Replace Trusted Executable 
- CAPEC 571 Block Logging to Central Repository 
- CAPEC 58 Restful Privilege Elevation 
- CAPEC 585 DNS Domain Seizure 
- CAPEC 586 Object Injection 
- CAPEC 589 DNS Blocking 
- CAPEC 590 IP Address Blocking 
- CAPEC 595 Connection Reset 
- CAPEC 598 DNS Spoofing 
- CAPEC 65 Sniff Application Code 
- CAPEC 75 Manipulating Writeable Configuration Files 
- CAPEC 88 OS Command Injection 
- CAPEC 89 Pharming 
- CAPEC 96 Block Access to Libraries 

# References
- [0] https://insinuator.net/2020/02/critical-bluetooth-vulnerability-in-android-cve-2020-0022/
- [1] https://capec.mitre.org/data/index.html
